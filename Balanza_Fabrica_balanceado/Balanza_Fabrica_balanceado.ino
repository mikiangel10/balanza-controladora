
/*
Programa para gestionar una Balanza
Escrito por Miguel Angel Gomez
20/12/2023


Agregada la funcion de Tara
Probar el funcionamiento con la siguiente rutina:
  Descargar la balanza
  Mantener la tecla B
  Listo

Probar la calibracion via teclado numerico. Procedimiento:
  Mantener apretada 'C'
  ingresar numero de celda (1)
  Apretar 'A'
  Descargar la Balanza
  Apretar 'A'
  Cargar la Balanza
  Ingresar Kilos cargados
  Apretar 'A'
  Listo

*/


#include <HX711_ADC.h>
#include <Keypad.h>
#include <Key.h>
#include <SoftPWM.h>
#include <SoftPWM_timer.h>
#include <EEPROM.h>
#include <LiquidCrystal.h>
#include <TimerOne.h>


#define led_lcd 34
#define salida1 46
#define salida2 44
#define salida3 42
#define salida4 40

#define MODO_MAX 1  //el numero de modo principal mayor posible

boolean listo = 0; //determinar si se termino una carga o programa
int meta = 0;
char motor = 0;
char salida[4] = {46, 44, 42, 40};
boolean estado_salida[4] = {0, 0, 0, 0};
int modo = 1; //0:modo programa,1: modo manual(MM); 21: MM-seleccionar motor; 22: MM-Cargando
String textoStr="0";
char error = 0;
char celdaCal;
char celdaCalAnt=10;
unsigned char led_lcd_pow = 50;
LiquidCrystal lcd(23, 25, 27, 29, 31, 33);

///keypad variables///
boolean hold = 0;
const char columnas = 4;
const char filas = 4;
unsigned char teclas[filas][columnas] = {{'1', '2', '3', 'A'}, {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'}, {'*', '0', '#', 'D'}
};
char pinesfilas[filas] = {A7, A6, A5, A4};
char pinescolumnas[columnas] = {A3, A2, A1, A0};
char key = 0;
Keypad Teclado(makeKeymap(teclas), pinesfilas, pinescolumnas, filas, columnas);

///load cell variables
//se usan las eeprom 10 a 21(0xa - 0x15)
//para los factores de calibracion
//se usa la direccion 0x16 para la cantidad de celdas
//se usan las direcciones 0x20 a 0x2b para
//los datos de tare offset
const char addCeldas = 0x16;  //Direccion de la cantidad de celdas a utilizar(1-3)
const char addOffset = 0X20;  //Primera Direccion donde se guarda el offset de las celdas
const char addCalVal= 10;     //direccion del primer valor de calibracion
HX711_ADC celdas[3] {HX711_ADC(50, 52), HX711_ADC(30, 32), HX711_ADC(26, 28)};
float peso = 0.0;
long tareOffset;
char cal_cel = 0; //celda a calibrar
unsigned char cantCeldas;
float pesoCalSerial = 0;


//variables para el TimerOne
int dSecCount = 0;
boolean oneSec = 0;
boolean oneDSec = 0;
void setup() {
  Serial.begin(9600);
  pinMode(led_lcd, OUTPUT);
  pinMode(salida[0], OUTPUT);
  pinMode(salida[1], OUTPUT);
  pinMode(salida[2], OUTPUT);
  pinMode(salida[3], OUTPUT);
  lcd.begin(16, 2);

  Timer1.initialize(100000);
  Timer1.attachInterrupt(countDSecCount);
  SoftPWMBegin();
  SoftPWMSet(led_lcd, 0);
  SoftPWMSetFadeTime(led_lcd, 1000, 1000);
  SoftPWMSet(led_lcd, led_lcd_pow);
  Teclado.addEventListener(keypadEvent);
  Teclado.setHoldTime(1000);
  Teclado.setDebounceTime(100);

  lcd.print("Balanzas   C J ");
  lcd.setCursor(0, 1);
  lcd.print("Iniciando...");

  EEPROM.get(addCeldas, cantCeldas); //se establece cuantas celdas usa la balanza
  if (cantCeldas < 1 || cantCeldas > 3) {
    cantCeldas = 1;
    EEPROM.put(addCeldas, cantCeldas);
    Serial.print(F("Se guardo en "));
    Serial.print(String(addCeldas, HEX));
    Serial.print(" el ");
    Serial.println(cantCeldas);
  }
  for (int n = 0; n < cantCeldas; n++) {
    celdas[n].begin();
    delay(800);
    char addo = addOffset + (n * sizeof(long));
    EEPROM.get(addo, tareOffset);
    Serial.print(F("TareOffset en EEPROM:"));
    Serial.println(tareOffset);
    celdas[n].start(50, false);
    if (celdas[n].getTareTimeoutFlag()) {
      lcd.setCursor(0, 1);
      lcd.print(F("Falla Celda "));
      lcd.print(n + 1);
      delay(5000);
      error = n + 1;
    } else {
      float cf;
      char add = addCalVal + (n * sizeof(float));
      EEPROM.get(add, cf);
      celdas[n].setCalFactor(cf);
      Serial.print(F("TareOffSet Seteado al inicio: "));
      Serial.println(celdas[n].getTareOffset());
      celdas[n].setTareOffset(tareOffset);
      Serial.print(F("TareOffSet ajustado: "));
      Serial.println(celdas[n].getTareOffset());
      Serial.print(F("Celda numero: "));
      Serial.println(n + 1);
      Serial.print(F("Direccion EEPROM: "));
      Serial.println(String(add, HEX));
      Serial.print("Coeficiente: ");
      Serial.println(String(cf));
      lcd.setCursor(0, 1);
      lcd.print("Celda ");
      lcd.print(n + 1);
      lcd.print(" OK  ");
      lcd.print(String(cf, 2));

    }

  }
  delay(2000);
  lcd.setCursor(0, 1);
  lcd.print("          ");
  lcd.setCursor(0, 0);
  lcd.print(F(" C J       P:"));
  lcd.print(textoStr);
  //
  Serial.print(F("Ingrese ´CAL´ para entrar en modo Calibracion")) ;


}

void loop() {
  key = Teclado.getKey();
  for (int n = 0; n < cantCeldas; n++) {
    celdas[n].update();
  }
  // if (Serial.available()){
  //   String inputStr=Serial.readString();
  //   Serial.print(inputStr);char
  // }
  switch (modo) {
    /**
      Modo 0 es Modo Programa
      Modo 1 es Modo manual-Se selecciona el peso y el motor y se carga hasta el peso seleccionado
      Modo 100 Pantalla de confirmacion de Tara de la Balanza
      Modo 101 Tarado de la Balanza
      Modo 121 es la pantalla de seleccion de motor para carga en modo manual
      Modo 21 se selecciona el motor
      Modo 122 se enciende el Motor
      Modo 22 es el proceso de carga en modo manual. finaliza cuando la carga llega a fin o hasta apretar "c"
      Modo 30 selecciona la calibracion de una o todas las celdas
      Modo 131 Seleccion de celda a calibrar
      Modo 31 es la pantalla de Tara en la calibracion para una celda
      Modo 132 Se tara la celda
      Modo 232 Se calcula el nuevo factor y se guarda todo
      Modo 32 es la pantalla de peso en la calibracion para una celda
      Modo 33 es la pantalla de Tara en la calibracion para toda la balanza
      Modo 34 es la pantalla de peso en la calibracion para toda la balanza
    **/
    case 0:  //modo programa

      lcd.setCursor(0, 0);
      lcd.print(F("Modo 0          "));
      break;

    case 1:  //modo manual
      celdaCalAnt=10;

      lcd.setCursor(0, 0);
      lcd.print(F("Inicio    Kg:     "));
      lcd.setCursor(12, 0);
      lcd.print(textoStr);
      //lcd.print(texto);
      lcd.setCursor(0, 1);
      lcd.print("Kg:");
      lcd.print(peso, 2);
      lcd.print(F("    "));

      imprimir_salidas();

      peso = actualizar_peso();
      if (oneSec) {
        Serial.print(peso);
        Serial.println(F(" KG."));
      }
      break;

    case 100: //Confirmacion de tara
      lcd.setCursor(1,0);
      lcd.print(F(" Confirme Tara  "));
      if (oneSec){
      Serial.println(F("Ingrese 'S' para confirmar la tara."));
       }
      break;

    case 101: //se tara la balanza y se guardan el offset
      for (int i =0; i<cantCeldas;i++){
        celdas[i].tare();
      }
      textoStr="0";
      modo=1;
      break;
    case 121://Se guarda el peso meta a cargar y se pide motor
      lcd.setCursor(0, 1);
      lcd.print(F("Motor?     "));
       //se establecen los kilos limites de la carga
      meta=textoStr.toInt();
      //meta=0;
      // for (int n = 0; n < (sizeof(texto)); n++) {
      //   if (texto[n] == ' ') {
      //     meta = 0;
      //   } else {
      //     meta = meta * 10;
      //     meta += (texto[n] - 0x30);
      //   }
      // }
      modo = 21;

      break;

    case 21: //seleccionando motor

      if (key < 0x40 && key > 0x29) {
        lcd.print(key);
        lcd.print(" ");
        motor = key;
        lcd.setCursor(6, 1);
        lcd.print(motor);
      }


      imprimir_salidas();
      break;

    case 122: //se enciende el motor
      if (motor > 0) {
        estado_salida[motor - 0x31] = 1;
        modo = 22;
      }
      break;

    case 22: //cargando hasta meta o cancelo

      peso = actualizar_peso();
      lcd.setCursor(0, 0);
      if (meta > 0) {
        if (peso >= meta) {
          apagar_salidas();
          motor = 0;
          modo = 1;
          break;
        }
        lcd.print(F("Carg.M:"));
        lcd.print(motor);
        lcd.print(" Kg:");
        lcd.print(meta);
        lcd.print(F("   "));
      } else {
        lcd.print(F("Cargando manual "));
      }
      lcd.setCursor(0, 1);
      lcd.print("Kg:");
      lcd.print(peso, 2);
      lcd.print(" ");
      imprimir_salidas();

      break;

    case 30:    //calibracion comienza
      //inico de calibración
      //dejar
      //Con A se acepta y pasa a estado 31
      //con C se sale
      //Con * se borra el numero de celda
      lcd.setCursor(0, 0);
      lcd.print(F("Num Celda a Cal.  "));
      lcd.setCursor(0, 1);

      if (textoStr.toInt()>cantCeldas)textoStr=("0");
      celdaCal=textoStr.toInt();
      //if (texto[sizeof(texto) - 1] > cantCeldas + 0x30) texto[sizeof(texto) - 1] = '0';
      //celdaCal = texto[sizeof(texto) - 1];

      lcd.print(celdaCal);
      lcd.setCursor(1, 1);
      lcd.print(F("    (0=Balanza)   "));
      if (celdaCal != celdaCalAnt) {
        celdaCalAnt = celdaCal;
        Serial.println(F("Ingrese numero de celda a calibrar o ´0' para calibrar toda la balanza:"));
        Serial.println(textoStr);
      }//inicio de calibracion
      break;

    case 131: //eleccion de celda a calibrar
      cal_cel=textoStr.toInt()-1;
      Serial.print(F("Se Calibrará la celda: "));
      Serial.println(cal_cel+1, DEC);
      Serial.println(F("Descargue la balanza y oprima 'S'"));
      // cal_cel = texto[sizeof(texto) - 1] - 0x31; //se elige una de las 3 celdas a calibrar
      celdas[cal_cel].setCalFactor(1.0);
      modo = 31;
      break;

    case 31: //tara de la balanza

      //Se debe descargar la Balanza
      //y despues presionar A para pasar a estado 32
      //Con C se cancela
      celdas[cal_cel].update();
      lcd.setCursor(0, 0);
      lcd.print(F ("Tarar Celda "));
      lcd.print(cal_cel + 1 );
      lcd.print(F("    "));
      lcd.setCursor(0, 1);
      lcd.print(F("Dato:"));
      peso = celdas[cal_cel].getData();
      lcd.print(peso);
      lcd.print(F("              "));
      if (oneSec) {
        Serial.print(F("Peso sensado: "));
        Serial.print(peso);
        Serial.println(F(" KG."));
      }
      break;

    case 132: //confirmacion de la tara
      Serial.print("Cargue la balanza e ingrese el peso");

      // for (int i = 0; i < sizeof(texto); i++) { //se elimina la variable 'texto'
      //   texto[i] = ' ';
      // }
      textoStr="0";
      celdas[cal_cel].tare();//se tara la celda
      tareOffset = celdas[cal_cel].getTareOffset();
      modo = 32;
      break;

    case 232:
      funcion1();
      modo = 1;
      break;

    case 33: //se calibran todas las celdas
      Serial.println(F("Se calibraran todas las celdas."));
      break;

    case 32: //carga de una masa conocida
      //se debe cargar la balanza con una masa conocida
      // e introducir el peso en el display
      celdas[cal_cel].update();
      lcd.setCursor(0, 0);
      lcd.print(F("Celda  "));
      lcd.setCursor(7, 0);
      lcd.print(cal_cel + 1);
      lcd.print(":");
      peso = celdas[cal_cel].getData();
      lcd.print(peso);
      lcd.print(F("        "));
      lcd.setCursor(0, 1);
      lcd.print(F("Carga:"));
      String textrim = textoStr;
      textrim.trim();
      lcd.print(textrim);
      lcd.print(F("Kg          "));
      if (oneSec) {
        Serial.print(peso,DEC);
        Serial.println(F(" es el dato sensado. Confirme con 'S'"));
        Serial.print(pesoCalSerial);
        Serial.println(F(" es el peso ingresado."));
      } //se introduce el peso en la celda
      break;


  }
  if (oneSec) { //rutina que se ejecuta cada 1 segundo
    oneSec = 0;
  }
  if (oneDSec) { //rutina que se ejecuta cada 100mS
    oneDSec = 0;
  }

}

void keypadEvent(KeypadEvent tecla) {
  switch (Teclado.getState()) {
    case HOLD:
      if (tecla == '*') { //si se mantiene ´*´
        led_lcd_pow = led_lcd_pow - 10; ///disminuye la luminosidad del display
        SoftPWMSet(led_lcd, led_lcd_pow);
      } else if (tecla == '#') { // si se mantiene ´#´
        led_lcd_pow = led_lcd_pow + 10; //aumenta la luminosidad del display
        SoftPWMSet(led_lcd, led_lcd_pow);
        //si se mantiene presionado un numero del 1 al 4 en modo manual
      } else if (tecla == '1' && modo == 21) { //seleccion de motor en modo manual
        estado_salida[0] = !estado_salida[0];
        modo = 22;
      } else if (tecla == '2' && modo == 21) { //seleccion de motor en modo manual
        estado_salida[1] = !estado_salida[1];
        modo = 22;
      } else if (tecla == '3' && modo == 21) { //seleccion de motor en modo manual
        estado_salida[2] = !estado_salida[2];
        modo = 22;
      } else if (tecla == '4' && modo == 21) { //seleccion de motor en modo manual
        estado_salida[3] = !estado_salida[3];
        modo = 22;
      } else if (tecla == 'C') { // si se mantiene presionado C
        modo = 30; //Entra en modo Calibracion

      }else if(tecla== 'B'){
        if (modo==1){//si esta en funcionamiento normal
          modo=100 ; //pasa a modo de tara
        }
      }
      hold = 1;
      break;
    case RELEASED:
      //al apretar  # se cambia entre modo manual y automatico
      if (tecla == '#' && !hold && modo <= MODO_MAX) {
        modo--;
        if (modo > MODO_MAX ) {
          modo = 0;
        } else if (modo < 0) {
          modo = MODO_MAX;
        }
      }

      hold = 0;
      break;
    case PRESSED:
      if (tecla == '*' ) { //si se presiona *
        // int n = 0;      // se borra un caracter de lo escrito
        // while (texto[n] == ' ' && n < sizeof(texto) - 1) {
        //   n++;
        // }
        // texto[n] = ' ';
        if (textoStr.length()==0){
          textoStr="0";
        }else {
          textoStr.remove(textoStr.length()-1);
        }

      } else if (tecla == 'C') { //si se presiona C
        if ( (modo == 22 || modo == 21)) { //se apagan todas las salidas que esten activas
          apagar_salidas();
          motor = 0;
          modo = 1;
        } else if (modo >= 30) { //o se vuelve al modo manual si esta en modo Calibracion
          modo = 1;
        }
      } else if (tecla == 'A' ) {
        if (modo == 1) {
          modo = 121;

        } else if (modo == 21) {
          modo = 122;

        } else if (modo == 30) { //menu de celda a calibrar
          //if (texto[sizeof(texto) - 1] == '0') { //si se ingresa 0 se calibra toda la balanza
          if( textoStr.toInt() == 0){
            modo = 33; //
          } else { //sino se elige cual se calibra
            modo = 131;
          }
        } else if (modo == 31) { //pantalla de tara de la celda
          modo = 132;
        } else if (modo == 32) {

          //pesoCalSerial = atof(texto);
          pesoCalSerial = textoStr.toFloat();
          modo = 232;

        }else if(modo==100){//en pantalla de tara
          modo=101; //se realiza la tara
        }

      } else if (modo == 0 || modo == 1 || modo == 30 || modo == 32) {

        if (tecla >= '0' || tecla <= '9') textoStr += tecla;
        // for (int n = '0'; n <= '9'; n++) {
        //   if (tecla == n) {
        //     for (int n1 = 0; n1 < sizeof(texto); n1++) {
        //       texto[n1] = texto[n1 + 1];
        //     }
        //     texto[sizeof(texto) - 1] = tecla;
        //   }
        // }
      }
      break;
  }

}

float actualizar_peso(void) {
  extern HX711_ADC celdas[3];
  float resultado = 0.0;
  for (int n = 0; n < cantCeldas; n++) {
    celdas[n].update();
    resultado += celdas[n].getData();
  }
  return resultado;
}

void imprimir_salidas(void) {
  lcd.setCursor(10, 1);
  lcd.print("S:");
  for (char n = 0; n < 4; n++) {
    digitalWrite(salida[n], !estado_salida[n]);
    lcd.setCursor(12 + n, 1);
    lcd.print(estado_salida[n]);
  }
}

void apagar_salidas(void) {
  for (int n = 0; n < 4; n++) {
    estado_salida[n] = 0;
  }

}

void countDSecCount() {
  oneDSec = 1;
  dSecCount++;
  if (dSecCount % 10 == 0) {
    oneSec = 1;
  }
}

void funcion1(void){ //funcion que ejecuta el codigo del modo 232
  Serial.print("OK");//se finaliza la calibracion y se guardan los valores
      Serial.print(F("Texto:"));

      // Serial.println(texto);
      Serial.println(textoStr);
      Serial.print(F("Peso:"));
      Serial.println(peso);
      Serial.print(F("Peso Serial:"));
      Serial.println(pesoCalSerial);
      celdas[cal_cel].refreshDataSet();
      float newCalVal= celdas[cal_cel].getNewCalibration(pesoCalSerial);
      celdas[cal_cel].setCalFactor(newCalVal);
      Serial.print(F("Numero de celda:"));
      Serial.println(cal_cel + 1, DEC);
      Serial.print(F("Nuevo factor de calibracion: "));
      Serial.println(String(newCalVal));
      char add = 10 + (cal_cel * sizeof(float));
      char addo = 0x20 + (cal_cel * sizeof(long));
      EEPROM.put(add, newCalVal);
      EEPROM.put(addo, tareOffset);
      Serial.println(String(add, HEX));
      Serial.println(String(addo, HEX));
      EEPROM.get(addo, tareOffset);
      EEPROM.get(add, newCalVal);
      Serial.print(F("El nuevo factor de calibracion es:"));
      Serial.println(String(newCalVal));
      Serial.print(F("Tare offset:"));
      Serial.println(String(tareOffset));
}

void serialEvent() {
  String inputStr = Serial.readString();
  inputStr.trim();
  inputStr.toUpperCase() ;
  Serial.print(F("Recibido: "));
  Serial.println(inputStr);
  if (inputStr == "CAL") {
    Serial.println("OK");
    modo = 30;
  }else if(inputStr=="TARA"){
    modo=100;
  }else if(inputStr== "CANCEL"){
    modo=1;
  } else if (modo == 30) {
    //MODO 131
    if (inputStr == "S") {
      //if (texto[sizeof(texto) - 1] == '0') { //si se ingresa 0 se calibra toda la balanza
      if (textoStr.toInt() == 0 ){
        modo = 33;
      } else { //se calibra una sola celda
        modo = 131;
      }
    } else {

      textoStr = inputStr;
      //texto[sizeof(texto) - 1] = inputStr.c_str()[0];
    }
  } else if (modo == 31) {

    inputStr.toUpperCase() ;
    if (inputStr == "S") {
      modo = 132;
    }
  } else if (modo == 32) {
    if (inputStr == "S") {
      modo = 232;
      Serial.print("232 Serie");
    } else {
      pesoCalSerial = inputStr.toFloat();
      Serial.print(F("Ingreso:"));
      Serial.println(pesoCalSerial);

      textoStr = inputStr;
      Serial.println(inputStr);
      //inputStr.getBytes(texto, sizeof(texto));
      //Serial.println(texto);
    }
  }else if(modo == 100){
    if (inputStr == "S") modo=101;
  }else{
    textoStr=inputStr;
  }
}
